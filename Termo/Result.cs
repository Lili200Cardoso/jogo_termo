namespace Game;

public class Result
{
    public bool Won = false;

    public LetterResult[] Letters;

    public Result(LetterResult[] letterResults)
    {
        this.Letters = letterResults;
        this.Won = this.Letters.All((l) => l == LetterResult.OK);
    }


}