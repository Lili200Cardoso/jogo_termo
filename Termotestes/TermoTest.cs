namespace Termotestes;

[TestClass]
public class TermoTestes
{
    [TestMethod]
    public void TestNormalRun()
    {
        Wordle wordle = new Wordle("URNAS");
        Result result = wordle.Play("VIUVA");

        Assert.AreEqual(false, result.Won);

        LetterResult[] letters = new LetterResult[]
        {
            LetterResult.Wrong,
            LetterResult.Wrong,
            LetterResult.Displaced,
            LetterResult.Wrong,
            LetterResult.Displaced

        };
        CollectionAssert.AreEqual(letters, result.Letters);


        result = wordle.Play("CASOU");

        Assert.AreEqual(false, result.Won);

        letters  = new LetterResult[]
        {
            LetterResult.Wrong,
            LetterResult.Displaced,
            LetterResult.Displaced,
            LetterResult.Wrong,
            LetterResult.Displaced

        };
        CollectionAssert.AreEqual(letters, result.Letters);


        result = wordle.Play("LUANA");

        Assert.AreEqual(false, result.Won);

        letters = new LetterResult[]
        {
            LetterResult.Wrong,
            LetterResult.Displaced,
            LetterResult.Displaced,
            LetterResult.Displaced,
            LetterResult.Displaced

        };
        CollectionAssert.AreEqual(letters, result.Letters);


        
        result = wordle.Play("USADA");

        Assert.AreEqual(false, result.Won);

        letters = new LetterResult[]
        {
            LetterResult.OK,
            LetterResult.Displaced,
            LetterResult.Displaced,
            LetterResult.Wrong,
            LetterResult.Displaced

        };
        CollectionAssert.AreEqual(letters, result.Letters);


        result = wordle.Play("URNAS");

        Assert.AreEqual(true, result.Won);

        letters = new LetterResult[]
        {
            LetterResult.OK,
            LetterResult.OK,
            LetterResult.OK,
            LetterResult.OK,
            LetterResult.OK

        };
        CollectionAssert.AreEqual(letters, result.Letters);


    }


}