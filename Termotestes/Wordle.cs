namespace Termotestes
{
    public class Wordle
    {

        private string Word;

        public Wordle(string word) => Word = word;

        public Result Play(string guess) 
            => new Result (guess.ToCharArray().Select((letter, index) => check(letter, index)).ToArray());

        private LetterResult check(char letter, int index)
        {
            var indexOf = Word.IndexOf(letter);
            if(indexOf == index)
            {
                return LetterResult.OK;
            }
            else if(indexOf >= 0)
            {
                return LetterResult.Displaced;
            }
            else
            {
                return LetterResult.Wrong;
            }
        }

    }
}